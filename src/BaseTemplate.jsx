import React from 'react'

export function BaseTemplate(props) {
    console.log('render base template')
    return (
        <>
            <div style={{ backgroundColor: 'aqua' }}>
                BASE TEMPLATE
            </div>
            {props.children}
        </>
    )
}