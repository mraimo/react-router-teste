import React from 'react'
import { useHistory } from 'react-router-dom'

export function HomePage(props) {
    const history = useHistory()
    return (
        <div>
            HOME PAGE
            <button onClick={() => history.push('/login')}>LOGIN</button>
        </div>
    )
}