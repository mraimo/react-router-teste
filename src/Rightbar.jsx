import React from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'

export function Rightbar (props) {
    console.log('render right sidebar')
    const history = useHistory()
    const match = useRouteMatch()
    return (
        <div style={{ backgroundColor: 'lightblue' }}>
            RIGHT BAR
            <button onClick={() => history.push(`/admin/group/${match.params.id}/events`)}>EVENTS</button>
            <button onClick={() => history.push(`/admin/group/${match.params.id}/announcements`)}>ANNOUNCEMENTS</button>
        </div>
    )
}