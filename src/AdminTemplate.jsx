import React from 'react'
import { Sidebar } from './Sidebar'
import { AdminRouter } from './AdminRouter'

export function AdminTemplate(props) {
    console.log('render admin template')
    return (
        <div style={{ display: 'grid', gridTemplateColumns: '200px auto', height: '100vh', width: '100vw' }}>
            <Sidebar />
            <AdminRouter />
        </div>
    )
}