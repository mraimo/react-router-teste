import React from 'react';
import './App.css';
import { Router } from './Router';

function App() {
  console.log('render app')
  return (
    <Router />
  );
}

export default App;
