import React from 'react'
import { GroupRouter } from './GroupRouter'
import { Rightbar } from './Rightbar'

export function GroupTemplate (props) {
    return (
        <div style={{ display: 'grid', gridTemplateColumns: 'auto 200px', height: '100vh' }}>
            <GroupRouter />
            <Rightbar />
        </div>
    )
}