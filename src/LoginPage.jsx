import React from 'react'
import { useHistory } from 'react-router-dom'

export function LoginPage (props) {
    console.log('render login page')
    const history = useHistory()
    return (
        <div>
            LOGIN PAGE
            <button onClick={() => history.push('/register')}>REGISTER</button>
            <button onClick={() => history.push('/admin')}>ADMIN</button>
        </div>
    )
}