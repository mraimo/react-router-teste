import React from 'react'
import { useHistory } from 'react-router-dom'

export function AdminHomePage (props) {
    console.log('render admin home page')
    const history = useHistory()
    return (
        <div>
            ADMIN HOME PAGE
            <button onClick={() => history.push('/admin/copy')}>COPY</button>
            <button onClick={() => history.push('/admin/group/ID')}>Group</button>
        </div>
    )
}