import React from 'react'
import { useHistory } from 'react-router-dom'

export function RegisterPage (props) {
    console.log('render register page')
    const history = useHistory()
    return (
        <div>
            REGISTER PAGE
            <button onClick={() => history.push('/login')}>LOGIN</button>
        </div>
    )
}