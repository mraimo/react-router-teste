import React from 'react'
import { useHistory } from 'react-router-dom'

export function AdminHomePageCopy (props) {
    console.log('render admin home page copy')
    const history = useHistory()
    return (
        <div>
            ADMIN HOME PAGE COPY
            <button onClick={() => history.push('/admin')}>ORIGINAL</button>
        </div>
    )
}