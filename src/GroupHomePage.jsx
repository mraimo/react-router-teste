import React from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'

export function GroupHomePage(props) {
    console.log('render group home page')
    const history = useHistory()
    const match = useRouteMatch()
    return (
        <div>
            GROUP HOME PAGE ({match.params.id})
            <button onClick={() => history.push(`/admin/group/${match.params.id}/events`)}>EVENTS</button>
            <button onClick={() => history.push(`/admin/group/${match.params.id}/announcements`)}>ANNOUNCEMENTS</button>
        </div>
    )
}