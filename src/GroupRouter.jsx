import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { GroupHomePage } from './GroupHomePage'
import { EventsPage } from './EventsPage'
import { AnnouncementsPage } from './AnnouncementsPage'

export function GroupRouter(props) {
    console.log('render group router')
    return (
        <Switch>
            <Route exact path="/admin/group/:id">
                <GroupHomePage />
            </Route>
            <Route path="/admin/group/:id/events">
                <EventsPage />
            </Route>
            <Route path="/admin/group/:id/announcements">
                <AnnouncementsPage />
            </Route>
        </Switch>
    )
}