import React from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'

export function EventsPage (props) {
    console.log('render events page')
    const history = useHistory()
    const match = useRouteMatch()
    return (
        <div>
            EVENTS (GROUP: {match.params.id})
            <button onClick={() => history.push(`/admin/group/${match.params.id}`)}>HOME PAGE</button>
        </div>
    )
}