import React from 'react'
import { useRouteMatch, useHistory } from 'react-router-dom'

export function AnnouncementsPage(props) {
    console.log('render announcements page')
    const history = useHistory()
    const match = useRouteMatch()
    return (
        <div>
            ANNOUNCEMENTS (GROUP: {match.params.id})
            <button onClick={() => history.push(`/admin/group/${match.params.id}`)}>HOME PAGE</button>
        </div>
    )
}