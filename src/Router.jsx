import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { BaseTemplate } from './BaseTemplate'
import { LoginPage } from './LoginPage'
import { HomePage } from './HomePage'
import { RegisterPage } from './RegisterPage'
import { AdminTemplate } from './AdminTemplate'

export function Router(props) {
    return (
        <Switch>
            <Route exact path="/">
                <HomePage />
            </Route>
            <BaseTemplate path="/login">
                <LoginPage />
            </BaseTemplate>
            <BaseTemplate path="/register">
                <RegisterPage />
            </BaseTemplate>
            <Route path="/admin">
                <AdminTemplate />
            </Route>
        </Switch>
    )
}