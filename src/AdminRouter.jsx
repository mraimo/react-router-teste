import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { AdminHomePage } from './AdminHomePage'
import { AdminHomePageCopy } from './AdminHomePage copy'
import { GroupTemplate } from './GroupTemplate'

export function AdminRouter(props) {
    console.log('render admin router')
    return (
        <Switch>
            <Route exact path="/admin">
                <AdminHomePage />
            </Route>
            <Route path="/admin/copy">
                <AdminHomePageCopy />
            </Route>
            <Route path="/admin/group/:id">
                <GroupTemplate />
            </Route>
        </Switch>
    )
}